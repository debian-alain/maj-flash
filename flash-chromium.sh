

#!/bin/bash

printf "\033c"

echo "------------------------------------------------------"
echo "       Mise à jour plugin FLASH pour Chromium         "
echo "------------------------------------------------------"
echo ""
echo ""
echo "Votre mot de passe vous sera demandé."
echo ""

read -rsp $'Appuyer sur une touche pour continuer...\n' -n1 ke
echo ""

set -e
arch=$(dpkg --print-architecture)
if [ "$(echo "$arch" | grep -c "^amd64$")" -eq 1 ];then
  arch="x86_64"
else
  arch="i386"
fi

UNPACKDIR=$(mktemp -d /tmp/pepperflashplugin-nonfree.XXXXXXXXXX)

## récuperation du numéro de la dernière version
infourl=https://www.adobe.com/software/flash/about
wget -q "$infourl" -O "$UNPACKDIR"/info.html
lignechromium=$( sed -n '/Chromium-based/=' "$UNPACKDIR"/info.html | sed '1q;d' )
ligneversion=$(( lignechromium + 2 ))
newversion=$(cat < "$UNPACKDIR"/info.html | sed "${ligneversion}q;d" | cut -d">" -f2 | cut -d"<" -f1)
installedversion=$(cat < /usr/lib/pepperflashplugin-nonfree/manifest.json | grep "version" | cut -d "\"" -f4)


if [ "$newversion" != "$installedversion" ] 
then
  echo "----------------------------------------------------------------------"
  echo "Votre version actuelle de flashplayer est : $installedversion"
  echo "Mise à jour flashplayer $installedversion => $newversion"
  echo "----------------------------------------------------------------------"
  echo ""
  echo ""
  read -rsp $'Appuyer sur une touche pour continuer...\n' -n1 ke
  echo ""
  
  ## Téléchargement de la version adaptée à votre architecture pour Chromium
  tarfile="flash_player_ppapi_linux"".$arch.""tar.gz"
  targzurl="https://fpdownload.adobe.com/pub/flashplayer/pdc/""$newversion""/$tarfile"
  wget -q -O "$UNPACKDIR/$tarfile" "$targzurl"

  cd "$UNPACKDIR" || exit
  ## extraction et installation 
  tar -xzf $tarfile
  sofile=$UNPACKDIR/libpepflashplayer.so
  if [ -e "$sofile" ]
  then
    sudo mv -f "$sofile" /usr/lib/pepperflashplugin-nonfree
    sudo chown root:root /usr/lib/pepperflashplugin-nonfree/libpepflashplayer.so
    sudo chmod 644 /usr/lib/pepperflashplugin-nonfree/libpepflashplayer.so
  fi

  jsonfile=$UNPACKDIR/manifest.json
  if [ -e "$jsonfile" ]
  then
    sudo mv -f "$jsonfile" /usr/lib/pepperflashplugin-nonfree
    sudo chown root:root /usr/lib/pepperflashplugin-nonfree/manifest.json
    sudo chmod 644 /usr/lib/pepperflashplugin-nonfree/manifest.json
  fi
  cd ..
  
  fpversion=$(cat < /usr/lib/pepperflashplugin-nonfree/manifest.json | grep "version" | cut -d "\"" -f4)
  echo "Votre version de flashplayer pour Chromium est maintenant: $fpversion."
else
  echo "Votre version flashplayer pour Chromium $installedversion est à jour."
fi

echo""
## nettoyage post installation
rm -rf "$UNPACKDIR"
