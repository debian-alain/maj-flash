
#!/bin/bash

printf "\033c"

echo "-----------------------------------------------------------"
echo "       Mise à jour plugin FLASH pour Mozilla Firefox       "
echo "-----------------------------------------------------------"
echo ""
echo ""
echo "Votre mot de passe vous sera demandé."
echo ""

read -rsp $'Appuyer sur une touche pour continuer...\n' -n1 ke
echo ""

set -e
arch=$(dpkg --print-architecture)
if [ "$(echo "$arch" | grep -c "^amd64$")" -eq 1 ];then
  arch="x86_64"
else
  arch="i386"
fi

FIREFOX_FLASH_INSTALL_DIR=${FIREFOX_FLASH_INSTALL_DIR:-/usr/lib/mozilla/plugins}

## récuperation du numéro de la dernière version
VERSION=$(wget -qO- https://fpdownload.macromedia.com/pub/flashplayer/masterversion/masterversion.xml | grep -m1 "NPAPI_linux version" | cut -d \" -f 2 | tr , .)

## Creation du dossier plugin s'il n'existe pas
if [ ! -e "$FIREFOX_FLASH_INSTALL_DIR" ]; then
  sudo mkdir -p "$FIREFOX_FLASH_INSTALL_DIR"
fi

## Vérification de la version actuellement installée
if [ -r "$FIREFOX_FLASH_INSTALL_DIR/libflashplayer.so" ]; then
  CUR_VER=$(strings $FIREFOX_FLASH_INSTALL_DIR/libflashplayer.so|grep LNX|awk '{print $2}' | tr , .)
  if [ "$CUR_VER" = "$VERSION" ]; then
    echo "Votre version flashplayer pour Mozilla Firefox $CUR_VER est à jour."
    echo""
    exit 0
  fi
fi


echo "----------------------------------------------------------------------"
echo "Votre version actuelle de flashplayer est : $CUR_VER."
echo "Mise à jour flashplayer $CUR_VER => $VERSION"
echo "----------------------------------------------------------------------"
echo ""
echo ""
read -rsp $'Appuyer sur une touche pour continuer...\n' -n1 ke
echo ""

cd /tmp

## Téléchargement de la version adaptée à votre architecture pour Mozilla Firefox
wget -q "https://fpdownload.adobe.com/pub/flashplayer/pdc/$VERSION/flash_player_npapi_linux.${arch}.tar.gz"

## Extraction du fichier lib dans le dossier plugin
if [ "$USER" = "root" ]; then
  tar -xof flash_player_npapi_linux.${arch}.tar.gz -C $FIREFOX_FLASH_INSTALL_DIR libflashplayer.so
else
  sudo tar -xof flash_player_npapi_linux.${arch}.tar.gz -C $FIREFOX_FLASH_INSTALL_DIR libflashplayer.so 
fi

## Suppression des fichiers temporaires
rm flash_player_npapi_linux.${arch}.tar.gz

## Fonction de récupération des utilisateurs .
listeusers () {
  UIDMINUSER=1000
  for LIGNES in $(getent passwd | cut -d":" -f1,3)
  do
  if [ "$(echo "$LIGNES" | cut -d":" -f2)" -ge "$UIDMINUSER" ] ;then
  echo "$LIGNES" | cut -d":" -f1
  fi
  done
}

## Pour chaque profile Firefox on supprime le fichier pluginreg.dat pour que Firefox affiche bien la bonne version de flash .
for user in $(listeusers) ; do  
  HOMEPCUSER=$(getent passwd "$user" | cut -d ':' -f6)
  if [ -f "$HOMEPCUSER"/.mozilla/firefox/profiles.ini ] ;then
    profileliste=$(cat < "$HOMEPCUSER"/.mozilla/firefox/profiles.ini | grep Path= | cut -d"=" -f2)
    for profilefirefox in $profileliste ; do
      if [ -d "$HOMEPCUSER"/.mozilla/firefox/"$profilefirefox"/ ];then
        echo "Suppression de : ""$HOMEPCUSER"/.mozilla/firefox/"$profilefirefox"/pluginreg.dat
        sudo rm -f "$HOMEPCUSER"/.mozilla/firefox/"$profilefirefox"/pluginreg.dat 2> /dev/null
      fi
    done
  fi
done

echo "Votre version de flashplayer pour Mozilla Firefox est maintenant: $VERSION"
echo ""

exit 0

 
